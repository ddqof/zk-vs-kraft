plugins {
    kotlin("jvm") version "1.8.21"
}

group = "me.ddqof"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.apache.kafka:kafka-clients:3.4.0")
    implementation("org.apache.zookeeper:zookeeper:3.6.3")
    implementation("com.github.docker-java:docker-java:3.3.1")
    implementation("org.testcontainers:testcontainers:1.18.1")
}

tasks.test {
    useJUnitPlatform()
}

kotlin {
    jvmToolchain(11)
}