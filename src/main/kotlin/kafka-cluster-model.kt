import com.github.dockerjava.api.DockerClient
import com.github.dockerjava.api.exception.NotModifiedException
import com.github.dockerjava.api.model.Container
import com.github.dockerjava.core.DockerClientBuilder
import org.apache.kafka.clients.admin.AdminClient
import org.apache.kafka.clients.admin.AdminClientConfig
import org.apache.kafka.clients.admin.NewTopic
import org.apache.zookeeper.ZooKeeper
import org.apache.zookeeper.server.DumbWatcher
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.testcontainers.containers.output.FrameConsumerResultCallback
import org.testcontainers.containers.output.OutputFrame
import org.testcontainers.containers.output.WaitingConsumer
import java.lang.Integer.parseInt
import java.lang.Thread.sleep
import java.time.Duration
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import java.util.*
import java.util.concurrent.CountDownLatch
import java.util.concurrent.ForkJoinPool
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException
import java.util.function.Predicate
import kotlin.math.min
import kotlin.system.measureTimeMillis

const val KAFKA_CONTAINER_OPERATION_TIMEOUT = 10L

private fun parseLogEntryTimestamp(logMessage: String): Long {
    return ZonedDateTime.parse(
        logMessage.substring(logMessage.indexOfFirst { it == '[' } + 1, logMessage.indexOfFirst { it == ']' }) + " UTC",
        DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss[,SSS] z")
    ).toInstant().toEpochMilli()
}

fun Container.getName(): String {
    return names.first()
}

fun Container.getKafkaNodeId(): Int {
    return parseInt(names[0].substringBeforeLast("-").last().toString())
}

fun DockerClient.stopKafkaContainer(container: Container) {
    stopContainerCmd(container.id)
        .withTimeout(Duration.ofMinutes(KAFKA_CONTAINER_OPERATION_TIMEOUT).toSeconds().toInt())
        .exec()
}

abstract class KafkaDockerCluster : AutoCloseable {
    companion object {
        private val LOG: Logger = LoggerFactory.getLogger(KafkaDockerCluster::class.java)
    }

    abstract val composeFileName: String
    abstract val kafkaBrokerStartedRegex: Regex
    abstract val controllerIsReadyRegex: Regex
    private val dockerClient: DockerClient = DockerClientBuilder.getInstance().build()
    abstract val name: String
    private val bootstrapServers: String = "localhost:19092,localhost:29092,localhost:39092"
    lateinit var adminClient: AdminClient
    private lateinit var kafkaContainers: List<Container>
    var topicsCount: Int = 0
    private var threadPoolExecutor = ForkJoinPool()

    fun start() {
        LOG.info("Starting $name Kafka cluster")
        composeStart()
        Runtime.getRuntime().addShutdownHook(Thread { composeDown() })
        startClusterInternal()
        adminClient = AdminClient.create(
            mapOf(
                AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG to bootstrapServers,
            )
        )
    }

    protected open fun startClusterInternal() {
        kafkaContainers = parseKafkaContainers(dockerClient.listContainersCmd().exec())
        LOG.info("Waiting for all Kafka Servers to start")
        waitForKafkaBrokersStart()
        LOG.info("All Kafka Servers were started")
    }

    override fun close() {
        adminClient.close()
        composeDown()
    }

    private fun composeDown() {
        Runtime.getRuntime().exec("docker compose -f $composeFileName down -t 600").waitFor()
    }

    private fun composeStart() {
        Runtime.getRuntime().exec("docker compose -f $composeFileName up -d").waitFor()
    }

    private fun parseKafkaContainers(containers: List<Container>): List<Container> {
        return containers.filter { it.names.first().matches(Regex(".*kafka.*")) }
    }

    private fun waitForKafkaContainerLogMessage(
        kafkaContainer: Container,
        since: Int = 0,
        predicate: Predicate<OutputFrame>
    ) {
        val waitingConsumer = WaitingConsumer()
        dockerClient.logContainerCmd(kafkaContainer.id)
            .withFollowStream(true)
            .withSince(since)
            .withStdErr(true)
            .withStdOut(true)
            .exec(FrameConsumerResultCallback().apply {
                addConsumer(OutputFrame.OutputType.STDOUT, waitingConsumer)
                addConsumer(OutputFrame.OutputType.STDERR, waitingConsumer)
            })
        waitingConsumer.waitUntil(predicate, KAFKA_CONTAINER_OPERATION_TIMEOUT, TimeUnit.MINUTES, 1)
    }

    private fun waitForKafkaContainerStart(kafkaContainer: Container, since: Int = 0) {
        LOG.info("Waiting for Kafka container ${kafkaContainer.getName()} start")
        try {
            waitForKafkaContainerLogMessage(kafkaContainer, since) { logMessage ->
                val started = kafkaBrokerStartedRegex.containsMatchIn(logMessage.utf8String)
                if (started) {
                    LOG.info("Kafka container ${kafkaContainer.getName()} is started: ${logMessage.utf8StringWithoutLineEnding}")
                }
                return@waitForKafkaContainerLogMessage started
            }
        } catch (timeoutException: TimeoutException) {
            LOG.error("Timeout for Kafka container start is exceeded")
        }
    }

    fun findControllerReadyTime(sinceEpochMilli: Long): Long {
        var timestampOnControllerReady: Long = 0
        val countDownLatch = CountDownLatch(1)
        waitForKafkaContainerLogMessage(
            kafkaContainers.find { it.getKafkaNodeId() == getActiveControllerId() }!!,
            (sinceEpochMilli / 1000).toInt()
        ) {
            val logmsg = it.utf8StringWithoutLineEnding
            val controllerIsReady = controllerIsReadyRegex.containsMatchIn(logmsg)
            if (controllerIsReady) {
                LOG.info("New controller is determined: $logmsg")
                timestampOnControllerReady = parseLogEntryTimestamp(logmsg)
                countDownLatch.countDown()
            }
            return@waitForKafkaContainerLogMessage controllerIsReady
        }
        if (timestampOnControllerReady.toInt() == 0) {
            throw Exception()
        }
        return timestampOnControllerReady
    }

    private fun waitForKafkaBrokersStart() {
        val startedKafkaBrokersCountDownLatch = CountDownLatch(kafkaContainers.size)
        kafkaContainers.map {
            Runnable {
                waitForKafkaContainerStart(it)
                startedKafkaBrokersCountDownLatch.countDown()
            }
        }.forEach { threadPoolExecutor.submit(it) }
        startedKafkaBrokersCountDownLatch.await()
    }

    fun createTestTopics(n: Int, batchSize: Int = 50) {
        val finalBatchSize = min(n, batchSize)
        LOG.info("Creating $n topics with batchSize=$finalBatchSize")
        val createdTopics = mutableSetOf<String>()
        repeat(if (n <= finalBatchSize) 1 else n / finalBatchSize) {
            val batch = mutableSetOf<String>()
            do {
                val uuid = UUID.randomUUID().toString()
                if (!createdTopics.contains(uuid)) {
                    batch.add(uuid)
                }
            } while (batch.size < finalBatchSize)
            adminClient.createTopics(batch.map { NewTopic(it, 10, 3) }).all().get()
            createdTopics.addAll(batch)
            topicsCount += batch.size
            LOG.info("${createdTopics.size}/${n} topics are created")
        }
        LOG.info("$n topics are successfully created")
    }

    data class StopControllerResult(
        val stoppedAtTimestampEpochMilli: Long,
        val stoppingContainer: Container
    )

    fun stopController(): StopControllerResult {
        val controller = kafkaContainers.find { it.getKafkaNodeId() == getActiveControllerId() }!!
        LOG.info("Current Kafka controller is ${controller.getName()}. Stopping it")
        val stoppedAtTimestamp = System.currentTimeMillis()
        dockerClient.stopKafkaContainer(controller)
        LOG.info("Kafka controller (${controller.getName()}) is stopped")
        return StopControllerResult(stoppedAtTimestamp, controller)
    }

    fun stopRandomBrokerNotController(): StoppedBrokerResult {
        val stoppedContainer = getRandomNotControllerContainer()
        LOG.info("Stopping ${stoppedContainer.names.first()} Kafka node")
        val stoppedBrokerResult = StoppedBrokerResult(
            KafkaClusterData(topicsCount, name),
            measureTimeMillis {
                dockerClient.stopKafkaContainer(stoppedContainer)
            },
            stoppedContainer
        )
        LOG.info("Kafka container ${stoppedContainer.names.first()} is stopped")
        return stoppedBrokerResult
    }

    protected open fun getRandomNotControllerContainer(): Container {
        val activeControllerId = getActiveControllerId()
        while (true) {
            val randomContainer = kafkaContainers.random()
            if (randomContainer.getKafkaNodeId() != activeControllerId) {
                return randomContainer
            }
        }
    }

    fun startBroker(
        container: Container,
        since: Int = (System.currentTimeMillis() / 1000).toInt()
    ) {
        LOG.info("Starting Kafka container ${container.getName()}")
        try {
            dockerClient.startContainerCmd(container.id).exec()
        } catch (e: NotModifiedException) {
            LOG.warn("Exception $e occurred during starting Kafka container ${container.getName()}. Retrying...")
            sleep(1000L)
            startBroker(container, since)
        }
        waitForKafkaContainerStart(container, since)
    }

    abstract fun getActiveControllerId(): Int

}

data class StoppedBrokerResult(
    val kafkaClusterData: KafkaClusterData,
    val shutdownTimeInEpochMillis: Long,
    val kafkaContainer: Container
)

abstract class KRaftCluster : KafkaDockerCluster() {
    override val kafkaBrokerStartedRegex = Regex("Kafka Server started")
    override val controllerIsReadyRegex = Regex("Processed completeActivation")

    override fun getActiveControllerId(): Int {
        return adminClient.describeMetadataQuorum().quorumInfo().get().leaderId()
    }
}

class KRaftMixedCluster : KRaftCluster() {
    override val composeFileName: String =
        javaClass.getResource("kraft-mixed-compose.yml")!!.path
    override val name = "KRaft (Mixed)"
}

class KRaftDedicatedCluster : KRaftCluster() {
    override val composeFileName: String =
        javaClass.getResource("kraft-dedicated-compose.yml")!!.path
    override val name = "KRaft (Dedicated)"

    override fun getRandomNotControllerContainer(): Container {
        val randomNotControllerContainer = super.getRandomNotControllerContainer()
        return if (adminClient.describeMetadataQuorum()
                .quorumInfo()
                .get()
                .voters()
                .find { it.replicaId() == randomNotControllerContainer.getKafkaNodeId() } == null
        ) randomNotControllerContainer else getRandomNotControllerContainer()
    }
}

class KafkaZkCluster : KafkaDockerCluster() {
    override val composeFileName: String =
        javaClass.getResource("kafka-zk-compose.yml")!!.path
    override val kafkaBrokerStartedRegex = Regex("\\[KafkaServer id=\\d+] started")
    override val controllerIsReadyRegex = Regex("Ready to serve as the new controller")
    override val name = "KafkaZK"
    private lateinit var zookeeper: ZooKeeper

    override fun startClusterInternal() {
        super.startClusterInternal()
        zookeeper = ZooKeeper("localhost:2181", 2000, DumbWatcher())
    }

    override fun close() {
        zookeeper.close()
        super.close()
    }

    override fun getActiveControllerId(): Int {
        return parseInt(getKafkaControllerId().toString())
    }

    private fun getKafkaControllerId(): Char {
        return String(
            zookeeper.getData("/controller", null, null)
        ).split(",")
            .find { it.contains("brokerid") }
            ?.last() ?: throw Exception()
    }
}

data class KafkaClusterData(val topicsCount: Int, val name: String)