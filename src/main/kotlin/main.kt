import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.concurrent.TimeUnit

const val REPEAT_COUNT = 5
val LOG: Logger = LoggerFactory.getLogger("Main")

fun main() {
    listOf(
        runAllTests(50),
        runAllTests(250),
        runAllTests(1_250),
        runAllTests(2_500),
        runAllTests(3_500)
    ).flatten().forEach { LOG.info(it.toString()) }
}

fun runAllTests(topicsCount: Int): List<TestResults> {
    return listOf(KRaftDedicatedCluster(), KRaftMixedCluster(), KafkaZkCluster()).map { kafkaCluster ->
        kafkaCluster.use {
            it.start()
            LOG.info("Creating $topicsCount topics for all ${it.name} Kafka cluster")
            it.createTestTopics(topicsCount)
            testControlledShutdown(it, topicsCount)
            testControllerSwitch(it, topicsCount)
        }
    }
}

fun testControllerSwitch(kafkaCluster: KafkaDockerCluster, topicsCount: Int): TestResults {
    LOG.info("Running 'Controller switch' test on ${kafkaCluster.name} cluster with ${topicsCount * 10} partitions")
    var warmup = true
    val testResults = (0..REPEAT_COUNT).map {
        with(kafkaCluster.stopController()) {
            val result =
                kafkaCluster.findControllerReadyTime(stoppedAtTimestampEpochMilli) - this.stoppedAtTimestampEpochMilli
            kafkaCluster.startBroker(this.stoppingContainer)
            if (warmup) {
                LOG.info("Warmup for 'Controller switch' test is completed")
                warmup = false
            } else {
                LOG.info(
                    "'Controller switch' [${it - 1}]-iteration result on ${kafkaCluster.name} is $result ${TimeUnit.MILLISECONDS} " +
                            "(${TimeUnit.MILLISECONDS.toSeconds(result)} ${TimeUnit.SECONDS})"
                )
            }
            result
        }
    }
    return TestResults("Controller switch", kafkaCluster, TimeResults(testResults.drop(1), TimeUnit.MILLISECONDS))
}

private fun testControlledShutdown(kafkaCluster: KafkaDockerCluster, topicsCount: Int): TestResults {
    LOG.info("Running 'Controlled shutdown' test on ${kafkaCluster.name} cluster with ${topicsCount * 10} partitions")
    var warmup = true
    val testResults = (0..REPEAT_COUNT).map {
        val result = kafkaCluster.stopRandomBrokerNotController()
        kafkaCluster.startBroker(result.kafkaContainer)
        if (warmup) {
            LOG.info("Warmup for 'Controlled shutdown' test is completed")
            warmup = false
        } else {
            LOG.info("'Controlled shutdown' [${it-1}]-iteration result on ${kafkaCluster.name} is ${result.shutdownTimeInEpochMillis} ${TimeUnit.MILLISECONDS} " +
                    "(${TimeUnit.MILLISECONDS.toSeconds(result.shutdownTimeInEpochMillis)} ${TimeUnit.SECONDS})")
        }
        return@map result.shutdownTimeInEpochMillis
    }
    return TestResults(
        "Controlled shutdown",
        kafkaCluster,
        TimeResults(testResults.drop(1), TimeUnit.MILLISECONDS)
    )
}

data class TestResults(val testName: String, val kafkaCluster: KafkaDockerCluster, val timeResults: TimeResults) {
    override fun toString(): String {
        return "[${kafkaCluster.name}, partitions=${kafkaCluster.topicsCount * 10}] $testName $timeResults"
    }
}

data class TimeResults(val results: Collection<Long>, val timeUnit: TimeUnit) {
    private fun min(): String = "${results.min()} $timeUnit"

    private fun avg(): String = "${results.average()} $timeUnit"

    private fun max(): String = "${results.max()} $timeUnit"

    override fun toString(): String {
        return "MIN: ${min()}; AVG: ${avg()}; MAX: ${max()}; Full results: $results $timeUnit"
    }
}